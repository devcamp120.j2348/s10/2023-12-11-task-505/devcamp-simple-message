const createCourseMiddleware = (req, res, next) => {
    console.log("Create Course Middleware");
    next();
}

const getAllCourseMiddleware = (req, res, next) => {
    console.log("Get All Course Middleware");
    next();
}

const getCourseByIDMiddleware = (req, res, next) => {
    console.log("Get Detail Course Middleware");
    next();
}

const updateCourseMiddleware = (req, res, next) => {
    console.log("Update Course Middleware");
    next();
}

const deleteCourseMiddleware = (req, res, next) => {
    console.log("Delete Course Middleware");
    next();
}

module.exports = {
    createCourseMiddleware,
    getAllCourseMiddleware,
    getCourseByIDMiddleware,
    updateCourseMiddleware,
    deleteCourseMiddleware
}
