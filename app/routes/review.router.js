const express = require("express");

const router = express.Router();

const reviewMiddleware = require("../middlewares/review.middleware");

router.get("/reviews", reviewMiddleware.getAllReviewMiddleware, (req, res) => {
    return res.send({
        message: "Get All Review"
    })
})

router.post("/reviews", reviewMiddleware.createReviewMiddleware, (req, res) => {
    return res.send({
        message: "Create Review"
    })
})

router.get("/reviews/:reviewid", reviewMiddleware.getReviewByIDMiddleware, (req, res) => {
    const reviewid = req.params.reviewid;

    return res.send({
        message: "Get Review ID = " + reviewid
    })
})

router.put("/reviews/:reviewid", reviewMiddleware.updateReviewMiddleware, (req, res) => {
    const reviewid = req.params.reviewid;

    return res.send({
        message: "Update Review ID = " + reviewid
    })
})

router.delete("/reviews/:reviewid", reviewMiddleware.deleteReviewMiddleware, (req, res) => {
    const reviewid = req.params.reviewid;

    return res.send({
        message: "Delete Review ID = " + reviewid
    })
})

module.exports = router; // Tương tự export router

